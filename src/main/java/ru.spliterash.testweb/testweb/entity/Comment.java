package ru.spliterash.testweb.testweb.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import ru.spliterash.testweb.testweb.Utils;

import javax.persistence.Entity;
import java.util.Calendar;
import java.util.Date;

@Entity
@Document("comments")
public class Comment {
    private String text;
    private String name;
    private Date date;

    public Comment() {
    }

    public Comment(String text, String name) {
        this.text = text;
        this.name = name;
        this.date = Calendar.getInstance().getTime();
    }

    public String getFormatedDate() {
        return Utils.formatDate(date);
    }

    public String getText() {
        return text;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "text='" + text + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
