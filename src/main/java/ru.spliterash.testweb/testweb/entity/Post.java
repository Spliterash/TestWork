package ru.spliterash.testweb.testweb.entity;

import org.apache.tomcat.util.codec.binary.Base64;
import org.apache.tomcat.util.codec.binary.StringUtils;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.multipart.MultipartFile;
import ru.spliterash.testweb.testweb.Utils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Entity
@Document(value = "posts")
public class Post {
    private byte[] file;
    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    private String id;
    private String text;
    private String name;
    private List<Comment> comments = new ArrayList<>();
    private Date date;

    public Post() {
    }

    public Post(String name, String text) {
        new Post(name, text, null);
    }

    public Post(String name, String text, MultipartFile file) {
        if (name != null)
            this.name = name;
        else
            this.name = "null";
        if (text != null)
            this.text = text;
        else
            this.text = "null";
        this.date = Calendar.getInstance().getTime();
        if (file!=null&&!file.isEmpty()) {
            try {
                this.file = file.getBytes();
            } catch (Exception e) {
                this.file = null;
            }
        }
    }

    public String getImage() {
        if (file == null)
            return "/images/NoImage.svg";
        else
            return "/image/"+id;
    }

    public String getFormatedDate() {
        return Utils.formatDate(date);
    }

    public String getPreviewText() {
        String[] split = this.text.split(" ");
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < split.length && i < 25; i++) {
            builder.append(split[i]).append(" ");
        }
        return builder.toString().trim() + "...";
    }

    public String getFormattedText() {
        String[] split = text.split("\n");
        StringBuilder builder = new StringBuilder();
        for (String s : split) {
            builder.append("<p>").append(s).append("</p>");
        }
        return builder.toString();
    }

    public List<Comment> getComments() {
        return comments;
    }

    @Override
    public String toString() {
        return "Post{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                ", name='" + name + '\'' +
                ", comments=" + comments +
                ", date=" + date +
                '}';
    }

    public String getText() {
        return text;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getDate() {
        return date;
    }

    public byte[] getImageByte() {
        return file;
    }
}