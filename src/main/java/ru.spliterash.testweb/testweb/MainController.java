package ru.spliterash.testweb.testweb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.spliterash.testweb.testweb.entity.Comment;
import ru.spliterash.testweb.testweb.entity.Post;
import ru.spliterash.testweb.testweb.repository.PostsRepository;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
public class MainController {
    @Autowired
    private PostsRepository postsRepository;


    @GetMapping("/")
    public String main(Map<String, Object> model) {
        List<Post> all = postsRepository.findAll();
        model.put("posts", all);
        model.put("title", "Главная");
        model.put("num", all.size());
        return "index";
    }

    @PostMapping(value = "/add")
    //public String addPost(@RequestParam MultiValueMap body) {
    public String addPost(Map<String, Object> model, @RequestParam String name, @RequestParam String text, @RequestParam(required = false, name = "file") MultipartFile file) {
        postsRepository.save(new Post(name, text, file));
        model.put("title", "Успешное добавление статьи на сайт");
        return "added";
    }


    @GetMapping("/add")
    public String addGet(Map<String, Object> model) {
        model.put("title", "Добавление статьи");
        return "add";
    }

    @GetMapping("/post/{id}")
    public String getPostContent(@PathVariable String id, Map<String, Object> model) {
        Post post = postsRepository.findById(id).orElse(null);
        return fillPost(post, model);
    }

    @GetMapping(value = "/image/{id}",headers = "Accept=image/jpeg, image/jpg, image/png, image/gif")
    public @ResponseBody byte[] getImage(@PathVariable String id) {
        Post post = postsRepository.findById(id).orElse(null);
        if(post==null){
            return null;
        }else{
            return post.getImageByte();
        }

    }

    @PostMapping("/post/{id}")
    public String getPostPost(@PathVariable String id, Map<String, Object> model, @RequestParam String name, @RequestParam String text) {
        Post post = postsRepository.findById(id).orElse(null);
        if (post != null) {
            post.getComments().add(new Comment(text, name));
            postsRepository.save(post);
        }
        return getPostContent(id, model);
    }

    public String fillPost(Post post, Map<String, Object> model) {
        if (post == null) {
            model.put("title", "Страница не найдена");
            return "404";
        }
        model.put("title", "Просмотр " + post.getName());
        model.put("post", post);
        return "post";
    }
}
