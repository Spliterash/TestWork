package ru.spliterash.testweb.testweb;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utils {
    public static String formatDate(Date date) {
        if (date == null)
            return "Неизвестно";
        else
            return new SimpleDateFormat("dd MMMM YYYY", new Locale("ru")).format(date);
    }
}
