package ru.spliterash.testweb.testweb;

import com.mongodb.MongoClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Collection;
import java.util.Collections;

@Configuration
@EnableMongoRepositories
public class MongoConfig extends AbstractMongoConfiguration {
    @Override
    public MongoClient mongoClient() {
        return new MongoClient("127.0.0.1");
    }

    @Override
    protected String getDatabaseName() {
        return "testweb";
    }

    @Override
    protected Collection<String> getMappingBasePackages() {
        return Collections.singletonList("ru.spliterash.testweb");
    }

    @Override
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongoClient(), "testweb");
    }
}