package ru.spliterash.testweb.testweb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.spliterash.testweb.testweb.entity.Post;

public interface PostsRepository extends MongoRepository<Post, String> {
}